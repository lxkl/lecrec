# Lecrec: a Lecture Recorder Based on FFmpeg

## System Requirements

This project is developed under Linux.
It will most probably not run anywhere else without modifications.
Depending on your hardware, any recent Linux distribution should do.

We are using Debian/bullseye, which is in the testing state at the moment,
but the newer kernel may be needed for some of our hardware.
You need FFmpeg, the MPV and VLC media players,
Ruby (see top of the file [`record`](record) for required libraries),
and, depending on your application, Video4Linux loopback support
(Debian packages `v4l2loopback-dkms` and `v4l2loopback-utils`).

*Update December 2020:* moving to openSUSE.
Debian is great while the stable version is enough.
Once the testing version is required, the fun ends.
It may work for a while, but there will be trouble down the road.
See [`setup-openSUSE`](setup-openSUSE) on how we get a fresh installation
of openSUSE Tumbleweed ready for use with the Lecrec project.

For FullHD, a strong hardware is required:
we use an AMD Ryzen 9 PC with 32 GB RAM.
However, when restricting to 720HD, a good laptop may do;
we used a Dell Precision 3520 before we got the PC.

## Overview

The program `record` records one input audio stream and one or more input video streams.
Three types of files can be written, codenamed *alpha*, *beta*, and *gamma*:

* *Alpha files*:
  each input video stream combined with the input audio stream is written to a file.
  So we will have as many alpha files as we have input video streams.
* *Beta file*:
  this type of file will only be written if there is more than one input video stream.
  Besides the input audio stream, the beta file
  has all the input video streams as separate video tracks,
  so we will have a file with _multiple video tracks_.
* *Gamma file*:
  this type of file will only be written if there is more than one input video stream.
  Besides the input audio stream, the gamma file has
  all the input video streams in _one video track_
  arranged in a vertical or horizontal stack.

Typically, there will be two input video streams:
(i) a lecturer writing on a blackboard or whiteboard,
(ii) a computer-based presentation (slides or hacking demonstrations).
Another setup is to cover two side-by-side blackboards or whiteboards with one camera each.

Suggestions on how to get the input streams into the computer:

* Audio: use a USB audio interface with XLR input
  and get the signal from the PA system already installed in the lecture room.
  Examples for good USB audio interfaces:
  [Focusrite Scarlett 2i2](https://focusrite.com/audio-interface/scarlett/scarlett-2i2)
  or [Behringer MIC500USB Tube Ultragain](https://www.behringer.com/product.html?modelCode=P0B4N).
* Video of lecturer: a good USB camera.
  Example: [Marshall CV610-U3-V2](http://www.marshall-usa.com/cameras/CV610-U3-V2/).
  The following cable can be used for connection when a longer distance needs to be bridged:
  [LINDY 8m USB 3.0 Active Extension Cable Pro](https://www.amazon.de/-/en/gp/product/B00DYWRQHQ).
* Video from computer: an HDMI splitter combined with an HDMI grabber.
  The splitter is required in order that the signal can also be sent to a projector in the lecture room.
  There are a lot of cheap devices out there that are supposed to do splitting and grabbing,
  but many of them have mixed reviews.
  I suggest using more expensive hardware,
  for example: [Blackmagic Atem Mini](https://www.blackmagicdesign.com/products/atemmini);
  this is an HDMI mixer, so it can do much more than splitting and grabbing,
  and it could be used to implement many other setups as well.

## Usage

Two files are required in the current directory:
the definition file [`defs.yaml`](defs.yaml)
and the config file [`config.yaml`](config.yaml),
of which examples are provided.
The former contains device and quality definitions, and defaults;
the latter contains configs: which devices and qualities actually to use,
and operational parameters.

If `record` is called without arguments and with a basename equal to `record`,
it will create, in the current directory,
symlinks to itself after the names of configs from `config.yaml`.
If `record` is called without arguments and with a basename not equal to `record`,
it will start itself inside a new terminal using the config
given by the basename of the symlink.
The only supported argument is `--config NAME`,
which will make `record` run in the current terminal and use the named config.

To end the recording, press `q`; this is just how to stop recording in FFmpeg.

## Definition File

### Qualities

* `name`: the quality is known by this name in the config file.
* `args`: FFmpeg arguments that define this quality.

### Devices

* `name`: the device is known by this name in the config file.
* `class`: class of device in the sense of FFmpeg's `-f`.
  Typical values are `pulse` for an audio device
  and `v4l2` for a video device.
* `init_cmd`: for `v4l2` devices.
  Shell command to be executed before this device is used.
  Use `%{device}` to refer to the path of the video device.
  This option is useful when a video device needs some initialization that FFmpeg cannot do
  or of which we do not know yet how to tell FFmpeg to do it;
  see for example: https://unix.stackexchange.com/q/612618/145107.
* `args_in`: additional FFmpeg arguments to be given before `-i`.
  This is useful to select an input format supported by the device.
  You can use `ffmpeg -f v4l2 -list_formats all -i DEVICE`
  to get a list of what the device supports,
  where `DEVICE` is something like `/dev/video0`.
  Some devices support MJPEG or H.264, for example.
* `clock`: if the device is using a `realtime` or a `monotonic` clock.
  The latter is the default for `v4l2` devices,
  and the former is the default for all others.
* `system`: how the device is known to your operating system.
  If the class is `pulse`, use the name given by `pacmd list-sources`.
  A _list_ of names can also be given with this class,
  and then the first name found in the output of `pacmd list-sources` is used.
  For video if the class is `v4l2`, use the basename of the corresponding file in `/dev/v4l/by-id`.
  For any other device classes, use whatever should be passed to FFmpeg's `-i` as-is.

### Defaults

There are global, loopback, and quality defaults.
Global defaults are used for the global part of a config,
loopback defaults are used for each loopback in a config,
and quality defaults are used for each quality in a config (see next section).

## Config File

Any number of configs may be given.
Each config has a global part, zero or more loopbacks, and zero or more qualities.

### Global Part

* `audio`: name of audio device to capture from, as per `defs.yaml`.
* `video`: list of names of video devices to capture from, as per `defs.yaml`.
* `resolution`: resolution for all input video streams.
* `framerate`: framerate for all input video streams.
* `monitor_quality`: name of quality used to feed a stream
  into `mpv` during recording to act as a monitor.
  Omit this item for no monitor.
* `monitor_spec`: what stream to feed into the monitor process: `alpha1`, `alpha2`, etc., or `gamma`.
* `no_kill_ffmpeg`: if not given,
  run `pkill -x -9 ffmpeg` and wait a few seconds before starting.
  The latter may be useful after a previous session crashed.
* `thread_queue_size`: passed to FFmpeg's `-thread_queue_size` option.

### Loopback Config

* `input_nr`:
  number of input video stream to be sent to the loopback video device.
  Audio will be suppressed, since it seems that loopback video devices cannot transport audio.
* `device_nr`: number of loopback device to send to.
* `monitor`: if given, run `mpv` on the loopback device.
  This may give a monitor with a lower latency than the other one.
  However, we sometimes observed strange timewarp effects in the video shown in this monitor.
  Incidentally, we discovered that
  these will typically disappear once the Zoom client is started on the same loopack device.
  (What also seems to help is not to use `asetpts`;
  see `mk_alpha_outputs` in [`record`](record).
  But without `asetpts`, there will be other problems.)
* `args`: FFmpeg arguments for loopbacking.

[See below](#loopbacking) for more on loopbacking.

### Quality Config

* `name`: name of quality, as per `defs.yaml`.
* `scale`: scale all video to this resolution.
  If the resolution given via `scale` is the same as the one given via `resolution` in the global part,
  then the former is ignored.
* `extra_args`: additional FFmpeg arguments for current quality.
* `streaming`: list of streaming specifications.
  A streaming specification has the form `src => dst`,
  where `src` is `alpha1`, `alpha2`, etc. for the input video streams
  or `gamma` for the gamma stream; and `dst` is a URL.
  For example: `gamma => rtmp://example.com/live/test`.
  [See below](#streaming) for more on streaming.
* `no_alpha`, `no_beta`, `no_gamma`:
  do not create alpha, beta, or gamma files.
  `no_gamma` will also effect the (internal) gamma stream not to be created;
  this option is particularly interesting to reduce CPU requirements.
* `outdir`: directory for output files.
* `stack_direction`: how to stack multiple video streams in the gamma stream:
  horizontally (`hstack`) or vertically (`vstack`).

## Compatibility

We recommend at this time: AAC as audio codec, H.264 as video codec,
YUV420P as pixel format, and MP4 as container format.
The files play fine on Linux using a standard video player,
such as the Totem Movie Player or [VLC Media Player](https://en.wikipedia.org/wiki/VLC_media_player).
The latter allows to play different video tracks of the same file in different windows at the same time,
which is interesting for our multi track beta files.
All this should work on any recent operating system;
indeed, the multi track features of VLC have been tested successfully also on MacOS.

At my work place, we use [Nextcloud](https://nextcloud.com) to distribute files.
So the main question is: do our video files play directly from the Nextcloud web interface?
They do, in most cases. Working combinations:

* Chromium, Chrome, and Firefox on Linux
* Chrome and Firefox on Android
* Safari on macOS and iOS
* Nextcloud client on iOS

## Streaming

The program `record` has a built-in streaming functionality
that streams to an RTMP server.
Clients can receive the stream via RTMP or via HLS or DASH.
Delay is lowest with RTMP, but HLS and DASH may be more reliable.
Audio and video always stay in sync, no matter how big the delay.

Working combinations on the client side:

* Linux PC: MPV may work better than VLC.
* MacBook with MacOS: HLS plays directly in Safari. VLC also works fine.
* iPad: HLS plays directly in Safari. VLC also works fine, and
  another good option is
  [Rocket Media Live TV Player](https://apps.apple.com/us/app/rocket-media-live-tv-player/id1319651409)
  and maybe
  [Live Player](https://apps.apple.com/us/app/live-player-media-streaming/id1099439153).

Our server setup roughly follows
[this tutorial](https://www.nginx.com/blog/video-streaming-for-remote-learning-with-nginx/).
Here are details for a server running Debian/buster:
```
apt-get install nginx libnginx-mod-rtmp
```
In `/etc/nginx/nginx.conf`, add the following to the `http` block;
it is suggested to use an include for this:
```
server {
  listen 591 default_server;
  listen [::]:591 default_server;
  server_name _;
  location / {
    root /var/opt/de.uni_kiel.math/streaming;
  }
  location /auth {
    if ($arg_pwd = 'super_secret') {
      return 200;
    }
    return 401;
  }
}
```
The port `591` and the path `/var/opt/de.uni_kiel.math/streaming` are examples.
Temporary playlists and videos will be stored under this path.
Replace `super_secret` with an actual password and `chmod g-o` the file.
Outside of the `http` block, add (also using an include, if you like):
```
rtmp {
  server {
    listen 1935;
    allow publish x.x.0.0/16;
    deny publish all;
    allow play all;
    chunk_size 4096;
    notify_method get;
    application live {
      on_publish http://localhost:591/auth;
      live on;
      interleave on;
      dash on;
      dash_path /var/opt/de.uni_kiel.math/streaming/live/dash;
      dash_fragment 3s;
      dash_playlist_length 18s;
      hls on;
      hls_path /var/opt/de.uni_kiel.math/streaming/live/hls;
      hls_fragment 3s;
      hls_playlist_length 18s;
    }
  }
}
```
If using a different port than `591`, change the `on_publish` line.
Also note that the path `/var/opt/de.uni_kiel.math/streaming/` occurs here again.
In the `allow publish` line, replace `x.x.0.0/16` with an actual subnet description,
to restrict from which IP addresses a stream can be sent to the server.
If users are expected to be in a certain subnet, change the `allow play` line.

A possible streaming specification would be `gamma => rtmp://example.com/live/test?pwd=super_secret`;
note that the password will be sent unencrypted.
Users can then use any of the following:
```
rtmp://example.com/live/test
http://example.com:591/live/hls/test.m3u8
http://example.com:591/live/dash/test.mpd
```

## Loopbacking

Most video devices can only be opened by one process at a time.
After FFmpeg opened such a video device, it cannot be opened by a streaming application.
But maybe you want to stream the video while recording,
using an external program, like the Zoom client, and not the built-in streaming functionality.
To this end, a Video for Linux loopback device can be used.
Config example:
```
loopbacks:
  - input_nr: 2
    device_nr: 50
```
This will duplicate the input video stream
given by the second `video` of this config into `/dev/video50`.
Loopback devices will be created by `record`;
to this end, first the `v4l2loopback` module is removed.

In our experiments, the Zoom client works well with the loopback device,
but BBB (BigBlueButton) running in a browser does not recognize the loopback device.
Consider using the built-in streaming functionality instead of an external program.

## Background Information

### Synchronization

Without extra measures, audio and video streams from different devices will be out of sync.
FFmpeg provides `-copyts`, which offers an approach to a solution, but no complete solution.
See the following posts for more background information:
https://superuser.com/questions/1584557/ffmpeg-synchronize-streams-from-two-webcams
and https://superuser.com/questions/1584945/ffmpeg-audio-start-time-way-off.
The program `record` achieves synchronization through a PTS transformation:
search for `@pts` in [`record`](record) to discover the details.
Our solution is not perfect, but should be good enough for most purposes.
If you have any better ideas, please let us know.

## License

Public Domain

Written in the time from September 2020 to January 2021 by Lasse Kliemann <<lasse@lassekliemann.de>>.
To the extent possible under law,
the author(s) have dedicated all copyright and related and neighboring rights
to this software to the public domain worldwide.
This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software.
If not, see http://creativecommons.org/publicdomain/zero/1.0/.
